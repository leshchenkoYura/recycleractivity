package com.example.newapp.presentation.activity.recycleractivity;


import com.example.newapp.Const;
import com.example.newapp.data.IUseCasePerson;
import com.example.newapp.data.UseCasePerson;
import com.example.newapp.presentation.router.Router;

public class RecyclerPresenter implements IRecyclerView.Presenter {
    private IRecyclerView.View view;
    private IUseCasePerson useCase;

    public RecyclerPresenter() {
        useCase = new UseCasePerson();
    }


    @Override
    public void onStartView(IRecyclerView.View view) {
        this.view = view;

    }

    @Override
    public void onClick() {
        view.read(useCase.read());
    }


    @Override
    public void init() {
        useCase.create();
        view.initAdapter(useCase.read());
    }

    @Override
    public void delete(int position) {
        Router.getInstance().stepFragmentDialog(Const.STEP_DIALOG_DELETE,useCase.getItem(position));
    }

    @Override
    public void read() {
        view.read(useCase.read());

    }

    @Override
    public void onStopView() {
        view = null;
        useCase = null;
    }
}
