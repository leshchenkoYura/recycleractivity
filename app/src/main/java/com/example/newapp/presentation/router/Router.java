package com.example.newapp.presentation.router;

import com.example.newapp.Const;
import com.example.newapp.R;
import com.example.newapp.data.Data;
import com.example.newapp.data.ItemPerson;
import com.example.newapp.presentation.activity.dialog.DialogDelete;
import com.example.newapp.presentation.base.BaseActivityView;

public class Router implements IRouter {
    private static IRouter instance;
    private BaseActivityView view;
    private DialogDelete dialogDelete;

    public Router() {
    }

    public static synchronized IRouter getInstance(){
        if (instance == null){
            instance = new Router();
        }
        return instance;
    }

    @Override
    public void onStart(BaseActivityView view) {
        this.view = view;
    }

    @Override
    public void onStop() {
        view = null;
    }

    @Override
    public <T extends Data> void stepActivity(String step, T obj) {

    }

    @Override
    public <T extends Data> void stepFragment(String step, T obj) {

    }

    @Override
    public <T extends Data> void stepFragmentDialog(String step, T obj) {
        switch (step) {
            case Const.STEP_DIALOG_DELETE:
                if (obj instanceof ItemPerson) {
                    view.transactionFragmentDialog(dialogDelete = DialogDelete.newInstance((ItemPerson) obj), R.id.content_dialog);
                }
        }
    }

    @Override
    public void closeFragmentDialog(String nameDialog) {
        switch (nameDialog){
            case Const.STEP_DIALOG_DELETE:
                if (dialogDelete != null) {
                    view.closeFragmentDialog(dialogDelete);
                }
        }
    }
}
