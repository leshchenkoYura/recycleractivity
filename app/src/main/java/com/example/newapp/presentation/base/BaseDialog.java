package com.example.newapp.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;

public abstract class BaseDialog<Binding extends ViewDataBinding> extends DialogFragment {
    private Binding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        attachFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
        startFragment();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected abstract void initView();

    protected abstract void attachFragment();

    protected abstract void startFragment();

    protected abstract void stopFragment();

    protected abstract void destroyFragment();

    protected abstract void pauseFragment();

    protected abstract void resume();


    protected Binding getBinding() {
        return binding;
    }

}
