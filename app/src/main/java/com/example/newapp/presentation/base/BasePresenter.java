package com.example.newapp.presentation.base;

public interface BasePresenter<View> {
    void onStartView(View view);
    void onStopView();
}
