package com.example.newapp.presentation.activity.recycleractivity;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.newapp.Const;
import com.example.newapp.R;
import com.example.newapp.data.ItemPerson;
import com.example.newapp.presentation.activity.dialog.DialogDelete;
import com.example.newapp.presentation.adapter.MyRecyclerAdapter;
import com.example.newapp.databinding.ActivityRecyclerBinding;
import com.example.newapp.presentation.base.BaseActivity;
import com.example.newapp.presentation.base.BasePresenter;
import com.example.newapp.presentation.router.Router;

import java.util.List;

public class RecyclerActivity extends BaseActivity<ActivityRecyclerBinding>
        implements IRecyclerView.View, DialogDelete.Listener {
    private MyRecyclerAdapter adapter;
private IRecyclerView.Presenter presenter;


    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
presenter = new RecyclerPresenter();
getBinding().setEvent(presenter);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_recycler;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        Router.getInstance().onStart(this);
        presenter.onStartView(this);
        presenter.init();
    }


    @Override
    public void initAdapter(List<ItemPerson> list) {
        adapter = new MyRecyclerAdapter(list, presenter);
        getBinding().rvExample.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        getBinding().rvExample.setAdapter(adapter);
    }

    @Override
    public void read(List<ItemPerson> list) {
            adapter.read(list);
    }

    @Override
    public void delete(int position) {
        adapter.delete(position);
    }

    @Override
    public void onClick(int pos) {

    }


    @Override
    public void deleteOk(int id) {
        toastLong("delete id ".concat(String.valueOf(id)));
        Router.getInstance().closeFragmentDialog(Const.STEP_DIALOG_DELETE);
    }

    @Override
    public void cancel() {
        Router.getInstance().closeFragmentDialog(Const.STEP_DIALOG_DELETE);
    }

    @Override
    public void transactionFragmentNoBackStack(Fragment fragment, int container) {

    }

    @Override
    public void transactionFragmentWithBackStack(Fragment fragment, int container) {

    }

    @Override
    public void transactionFragmentDialog(DialogFragment fragment, int container) {
        super.transactionFragmentDialog(fragment,container);
    }

    @Override
    public void closeFragmentDialog(DialogFragment fragment) {
        super.closeFragmentDialog(fragment);
    }
}