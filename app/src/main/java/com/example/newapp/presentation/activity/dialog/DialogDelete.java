package com.example.newapp.presentation.activity.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.newapp.R;
import com.example.newapp.data.ItemPerson;
import com.example.newapp.presentation.activity.recycleractivity.RecyclerActivity;

public class DialogDelete extends DialogFragment {
    private static final String TAG_DATA = "data";
    private Listener listener;
    private ItemPerson person;

    public DialogDelete() {

    }

    public interface Listener{
        void deleteOk(int id);

        void cancel();
    }


    public static DialogDelete newInstance(ItemPerson person) {
        DialogDelete fragment = new DialogDelete();
        Bundle args = new Bundle();
        args.putParcelable(TAG_DATA,person);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (RecyclerActivity)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_dialog_delete, container, false);
        AppCompatButton ok = view.findViewById(R.id.btnOk);
        AppCompatButton cancel = view.findViewById(R.id.btnCancel);
        AppCompatTextView info = view.findViewById(R.id.tvPerson);

        if (getArguments() != null){
            person = getArguments().getParcelable(TAG_DATA);
            if (person != null){
                info.setText("id\t"
                        .concat(String.valueOf(person.getId()))
                        .concat("\n")
                        .concat("FirstName\t".concat(person.getFirstName()))
                        .concat("\n")
                        .concat("SecondName\t".concat(person.getSecondName())));
            }
        }
        ok.setOnClickListener(v -> {
            if (listener != null && person != null)listener.deleteOk(person.getId());
        });

        cancel.setOnClickListener(v -> {
            if (listener != null) listener.cancel();
        });
        return view;
    }
}