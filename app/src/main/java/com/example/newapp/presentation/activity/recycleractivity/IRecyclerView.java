package com.example.newapp.presentation.activity.recycleractivity;


import com.example.newapp.data.ItemPerson;
import com.example.newapp.presentation.base.BaseActivityView;
import com.example.newapp.presentation.base.BasePresenter;

import java.util.List;

public interface IRecyclerView {
    interface View extends BaseActivityView {
        void initAdapter(List<ItemPerson> list);

        void read(List<ItemPerson> list);

        void delete(int position);
        void onClick(int pos);

    }

    interface Presenter extends BasePresenter<View> {
        void onClick();

        void init();

        void delete(int position);

        void read();

    }
}
