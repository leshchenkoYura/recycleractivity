package com.example.newapp.presentation.activity.startactivity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.newapp.R;
import com.example.newapp.databinding.ActivityStartBinding;
import com.example.newapp.presentation.base.BaseActivity;
import com.example.newapp.presentation.base.BasePresenter;

public abstract class MyStartActivity extends BaseActivity<ActivityStartBinding> implements MyStartView.View {
    private MyStartView.Presenter presenter;


    @Override
    protected void initView(@Nullable Bundle savedInstanceState) {
        presenter = new MyStartPresenter();
        getBinding().setEvent(presenter);

    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_start;
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startView() {
        presenter.onStartView(this);
    }



}