package com.example.newapp.presentation.router;

import androidx.fragment.app.DialogFragment;

import com.example.newapp.data.Data;
import com.example.newapp.presentation.base.BaseActivityView;

public interface IRouter {
    void onStart(BaseActivityView view);

    void onStop();

    <T extends Data>void stepActivity(String step,T obj);

    <T extends Data>void stepFragment(String step,T obj);

    <T extends Data>void stepFragmentDialog(String step, T obj);

    void closeFragmentDialog(String nameDialog);
}
