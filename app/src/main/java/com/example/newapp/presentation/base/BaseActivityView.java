package com.example.newapp.presentation.base;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

public interface BaseActivityView {
    void transactionFragmentNoBackStack(Fragment fragment, int container);

    void transactionFragmentWithBackStack(Fragment fragment, int container);

    void transactionFragmentDialog(DialogFragment fragment, int container);

    void closeFragmentDialog(DialogFragment fragment);

}
