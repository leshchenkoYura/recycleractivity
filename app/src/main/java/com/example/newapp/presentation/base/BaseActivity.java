package com.example.newapp.presentation.base;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.example.newapp.Const;
import com.example.newapp.R;

import java.util.Objects;



public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {
    private Binding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());
        initView(savedInstanceState);
    }

    protected abstract void initView(@Nullable Bundle savedInstanceState);

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract BasePresenter getPresenter();

    protected abstract void startView();

    protected Binding getBinding(){
        return binding;
    }

    @Override
    protected void onStart() {
        super.onStart();
        startView();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (getPresenter() != null){
            getPresenter().onStopView();
        }
        super.onDestroy();
    }

    protected <T>void transactionActivity(Class<?> activity,boolean cycleFinish,T object){
        if (activity != null) {
            Intent intent = new Intent(this, activity);
//            if(object != null){
//                if(object instanceof Data){
//                    intent.putExtra(Const.TAG_DATA,(Data)object);
//                }
//            }
            startActivity(intent);
            if(cycleFinish) {
                this.finish();
            }
        }
    }

    protected void transactionFragmentNoBackStack(Fragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
//                .setCustomAnimations(R.anim.fragment_enter, R.anim.fragment_exit,
//                        R.anim.fragment_pop_enter, R.anim.fragment_pop_exit)
                .replace(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }

    protected void transactionFragmentWithBackStack(Fragment fragment, int container) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    protected void transactionFragmentDialog(DialogFragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
                .add(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }


    protected void closeFragmentDialog(DialogFragment fragment){
        this.getSupportFragmentManager()
                .beginTransaction()
                .remove(fragment)
                .commit();
    }

    protected void removeAllFragment(){
        if(getSupportFragmentManager() != null && getSupportFragmentManager().findFragmentById(R.id.content) != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(Objects.requireNonNull(getSupportFragmentManager().findFragmentById(R.id.content)))
                    .commit();
        }
    }

    protected void toastLong(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }
}
