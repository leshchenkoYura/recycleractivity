package com.example.newapp.presentation.adapter;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.newapp.data.ItemPerson;
import com.example.newapp.databinding.ItemBinding;
import com.example.newapp.presentation.activity.recycleractivity.IRecyclerView;


public class MyRecyclerHolder extends RecyclerView.ViewHolder {
private IRecyclerView.Presenter presenter;
private Context context;
private ItemBinding binding;


    public MyRecyclerHolder(@NonNull View itemView, Context context, IRecyclerView.Presenter presenter) {
        super(itemView);
        this.context = context;
        this.presenter = presenter;
        binding = DataBindingUtil.bind(itemView);
    }

    public void bind(int position, ItemPerson item){
        if (item != null && item.getFirstName() != null && !item.getFirstName().isEmpty()){
            binding.setItem(item);
            binding.cLayout.setOnLongClickListener(v ->{
                presenter.delete(position);
                return false;
            });
        }
    }
}
