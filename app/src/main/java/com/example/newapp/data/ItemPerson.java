package com.example.newapp.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.newapp.BuildConfig;


public class ItemPerson implements Parcelable,Data {
    private int id;
    private String firstName;
    private String secondName;

    public ItemPerson(int id, String firstName, String secondName) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
    }

    protected ItemPerson(Parcel in) {
        id = in.readInt();
        firstName = in.readString();
        secondName = in.readString();
    }

    public static final Creator<ItemPerson> CREATOR = new Creator<ItemPerson>() {
        @Override
        public ItemPerson createFromParcel(Parcel in) {
            return new ItemPerson(in);
        }

        @Override
        public ItemPerson[] newArray(int size) {
            return new ItemPerson[size];
        }
    };

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(firstName);
        dest.writeString(secondName);
    }

    @Override
    public String toString() {
        return BuildConfig.DEBUG
                ? "ItemPerson{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                '}' : "";
    }
}
