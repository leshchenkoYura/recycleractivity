package com.example.newapp.data;

import java.util.List;

public interface IUseCasePerson {
    void create();

    List<ItemPerson> read();

    ItemPerson update(ItemPerson person);

    int delete(int id);

    ItemPerson getItem(int position);
}
