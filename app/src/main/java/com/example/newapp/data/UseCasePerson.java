package com.example.newapp.data;

import java.util.ArrayList;
import java.util.List;

public class UseCasePerson implements IUseCasePerson {
    private List<ItemPerson> list;

    public UseCasePerson() {
        list = new ArrayList<>();
    }

    @Override
    public void create() {
        list.add(new ItemPerson(1,"Ivan","Batman"));
        list.add(new ItemPerson(2,"Vasiliy","SpiderMan"));
        list.add(new ItemPerson(3,"Petro","SuperMan"));
        list.add(new ItemPerson(4,"Nikolay","IronMan"));
        list.add(new ItemPerson(5,"Igor","Thor"));
        list.add(new ItemPerson(6,"Andriy","Hulk"));
        list.add(new ItemPerson(7,"Anna","SuperGirl"));
        list.add(new ItemPerson(8,"Lucy","SuperWoman"));
        list.add(new ItemPerson(9,"Victoria","CatWoman"));
        list.add(new ItemPerson(10,"Iren","Wasp"));
    }

    @Override
    public List<ItemPerson> read() {
        return list;
    }

    @Override
    public ItemPerson update(ItemPerson person) {
        return null;
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public ItemPerson getItem(int position) {
        return list.get(position);
    }
}
